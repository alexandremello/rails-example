Rails.application.routes.draw do
   post '/users/:user_id/check', to: 'user_check#check'
   get '/users/:user_id/balance', to: 'user_balance#balance'
end
