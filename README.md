
# README

## Coding Challenge

> You are tasked with writing a piece of software to do Checks and
> Balances.
> 
> It must comprise a HTTP Server with two endpoints:
> 
> * One to insert a new monetary transaction, money in or out, for a given user;
> * One to return a user's current balance.
> 
> You may use whatever language and platform with which you are most
> comfortable.

This challenge was resolved using Ruby 2.5.3 and Rails 5.2.2 (sorry about the rails mess) using *in memory cache* as storage for simplicity.

For test purpose a data seed was made and is applied when the application starts. This data seed is defined by `config/initializers/user_balances.rb` and creates 3 user accounts:
 - user_id: 1, balance: 1940.39
 - user_id: 2, balance: 200.00
 - user_id: 3, balance: 0
 
 Feel free to mess around with this data seed.

## How to execute it
There is a `Dockerfile` on the project root that can be used to execute this applications. To do that you need first to build the docker image and than execute it:
```
docker build -t challenge .
docker run -it --rm -p 3000:3000 challenge
```
### Running unit test
`RSpec` framework was used for the unit tests. To run unit tests, execute `rspec`.

For simplicity purpose, unit tests are run before on Docker container.

## API documentation

### User account balance
Retrieves the user account balance.

    GET /users/:user_id/balance
Returns the user balance:

    {"user_id":"1","balance":1940.39}
Errors:
 - `404` - User not found
 - `500` - Internal Server Error

### Check to user account
Add check to user account. 
To add a credit to the account, `amount` must be a positive value. 
To add a debit to the account, `amount` must be a negative value.

    POST /users/:user_id/check
Request body:

    { "amount": -20.0 }
Errors:
 - `404` - User not found
 - `500` - Internal Server Error
 - `422` - Non Sufficient Founds
 - `422` - Request params validation error

## Responsibility Separation
The challenge was done using the following responsibility separation:
 - controllers
 - use cases
 - models
 - repositories
 - serializers

### Controllers 
Responsible for type validation on request params, forward them to the proper **Use Case** and to respond the request. Handling errors if any.

### Use Cases
Contains the business logic and knows what needs to be done to fulfil the action request.

### Models
Internal classes to transport data between responsibility layer.

### Repositories
Know how to save and retrieve data from the storage.

### Serializers
Encapsulates the presentation logic for the API responses. Including error types.

## Error Handling
Repositories can raise `repository error types` that must be `rescued` by the classes that uses a repository as dependency. In this case, *use case*.
This statement is also valid to the others *responsibility layers*. Exceptions your dependencies would raise must be `rescued` and should not leak to outer context.

