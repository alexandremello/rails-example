require "rails_helper"

RSpec.describe UserBalanceSerializer do
   let(:described) { described_class.new }
   let(:user_id) { 1 }
   let(:user_balance) { 3383.2 }
   let(:balance) { Balance.new(user_id, user_balance) }

   describe "#serialize" do
      subject { described.serialize(balance) }

      it "serializes user_id and balance" do
         expect(subject).to eq({
            user_id: user_id,
            balance: user_balance
         })
      end
   end
end
