require "rails_helper"

RSpec.describe UserAccountRepository do
   let(:storage) { spy "storage" }
   let(:described) { described_class.new(storage: storage) }
   let(:user_id) { 1 }
   let(:balance) { 333.44 }
   let(:amount) { 13.35 }

   describe "#add_check_by_user_id" do

      before do
         allow(storage).to receive(:read).and_return(balance)
      end

      subject { described.add_check_by_user_id(user_id, amount) }

      it "add a transaction into user account" do
         user_balance = subject

         expect(storage).to have_received(:write).with(user_id, balance + amount)
         expect(user_balance.balance).to eq(balance + amount)
         expect(user_balance.user_id).to eq(user_id)
      end

      context "when the given user is not found" do
         before do
            allow(storage).to receive(:read).and_return(nil)
         end

         it "raises UserNotFound error" do
            expect { subject }.to raise_error(::RepositoryErrors::NotFound)
         end
      end
   end

   describe "#balance_by_user_id" do
      it "fetch the user from storage" do
         described.balance_by_user_id(user_id)

         expect(storage).to have_received(:read).with(user_id)
      end

      context "when the user is not found" do
         before do
            allow(storage).to receive(:read).and_return(nil)
         end

         it "raises not found error" do
            expect { described.balance_by_user_id(user_id) }.to raise_error(::RepositoryErrors::NotFound)
         end
      end
   end
end
