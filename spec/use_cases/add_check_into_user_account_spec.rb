require "rails_helper"

RSpec.describe AddCheckIntoUserAccount do
   let(:repository) { spy "repository" }
   let(:described) { described_class.new(repository: repository) }
   let(:user_balance) { 2324 }
   let(:user_id) { 2 }
   let(:amount) { 100 }

   describe "#add_check_by_user_id" do
      before do
         allow(repository).to receive(:add_check_by_user_id).and_return(user_balance)
      end

      subject { described.add_check_into_user_account(user_id, amount) }

      it "updates balance with check" do
         subject

         expect(repository).to have_received(:add_check_by_user_id).with(user_id, amount)
      end

      context "when the given user is not found" do
         before do
            allow(repository).to receive(:add_check_by_user_id).and_raise(::RepositoryErrors::NotFound.new("error"))
         end

         it "raises User Not Found error" do
            expect { subject }.to raise_error(::AddCheckIntoUserAccount::UserNotFound)
         end
      end
   end
end
