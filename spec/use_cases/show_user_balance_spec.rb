require "rails_helper"

RSpec.describe ShowUserBalance do
   let(:repository) { spy "repository" }
   let(:described) { described_class.new(repository: repository) }
   let(:user_id) { 333 }
   let(:user_balance) { 100000 }

   describe "#balance_by_user_id" do
      before do
         allow(repository).to receive(:balance_by_user_id).and_return(user_balance)
      end

      subject { described.balance_by_user_id(user_id) }

      it "fetch the user balance from repository" do
         expect(subject).to eq user_balance
         expect(repository).to have_received(:balance_by_user_id).with(user_id)
      end

      context "when repository raises NotFound error" do
         before do
            allow(repository).to receive(:balance_by_user_id).and_raise(::RepositoryErrors::NotFound.new("not found!"))
         end

         it "raises UserNotFound error" do
            expect { subject }.to raise_error(::ShowUserBalance::UserNotFound)
         end
      end
   end
end
