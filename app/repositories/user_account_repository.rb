class UserAccountRepository
   def initialize(storage: Rails.cache)
      @storage = storage
   end

   def add_check_by_user_id(user_id, amount)
      user_balance = balance_by_user_id(user_id)

      updated_balance = add_check(user_balance, amount)

      @storage.write(user_id, updated_balance.balance)

      updated_balance
   end

   def balance_by_user_id(user_id)
      user_balance = @storage.read(user_id)

      raise ::RepositoryErrors::NotFound.new("user #{user_id}") unless user_balance

      Balance.new(user_id, user_balance)
   end

   private

   def add_check(balance, amount)
      Balance.new(balance.user_id, balance.balance + amount)
   end
end
