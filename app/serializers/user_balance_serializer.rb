class UserBalanceSerializer
   def serialize(balance)
      {
         user_id: balance.user_id,
         balance: balance.balance,
      }
   end
end
