class ErrorSerializer
   def serialize(request_error)
      { error: request_error.message }
   end
end
