class Balance
   attr_reader :user_id, :balance

   def initialize(user_id, balance)
      @user_id = user_id
      @balance = balance
   end
end
