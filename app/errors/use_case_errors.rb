module UseCaseErrors
   NON_SUFFICIENT_FUNDS = "non sufficient funds for"

   class UserNotFound < ::Errors::NotFound
      def initialize(obj)
         super("user #{obj}")
      end
   end

   class NonSufficientFunds < ::Errors::GenericError
      def initialize(amount, message = NON_SUFFICIENT_FUNDS)
         @message = "#{message} #{amount}"
      end
   end
end
