module RequestErrors
   class UnprocessableEntity < ::Errors::GenericError; end
   class NotFound < ::Errors::GenericError; end
   class InternalServerError < ::Errors::GenericError; end
end
