module Errors
   NOT_FOUND_MESSAGE = "not found"

   class GenericError < StandardError
      attr_reader :message

      def initialize(message)
         @message = message
      end
   end

   class NotFound < ::Errors::GenericError
      def initialize(obj, message = NOT_FOUND_MESSAGE)
         @message = "#{obj} #{message}"
      end
   end
end
