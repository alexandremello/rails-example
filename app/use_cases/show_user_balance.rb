class ShowUserBalance
   class UserNotFound < ::Errors::GenericError; end

   def initialize(repository: ::UserAccountRepository.new)
      @repository = repository
   end

   def balance_by_user_id(user_id)
      @repository.balance_by_user_id(user_id)
   rescue ::RepositoryErrors::NotFound => error
      raise UserNotFound.new(error.message)
   end
end
