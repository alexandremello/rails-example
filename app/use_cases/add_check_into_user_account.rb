class AddCheckIntoUserAccount
   class UserNotFound < ::UseCaseErrors::UserNotFound; end
   class NonSufficientFunds < ::UseCaseErrors::NonSufficientFunds; end

   def initialize(repository: ::UserAccountRepository.new)
      @repository = repository
   end

   def add_check_into_user_account(user_id, amount)
      user_balance = @repository.balance_by_user_id(user_id)
      remaining_balance = user_balance.balance + amount

      raise NonSufficientFunds.new(amount) unless remaining_balance > 0

      @repository.add_check_by_user_id(user_id, amount)
   rescue ::RepositoryErrors::NotFound
      raise UserNotFound.new(user_id)
   end
end
