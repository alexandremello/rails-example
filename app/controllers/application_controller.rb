class ApplicationController < ActionController::API
   rescue_from ::StandardError, with: :internal_server_error
   rescue_from ::RequestErrors::UnprocessableEntity, with: :unprocessable_entity
   rescue_from ::RequestErrors::NotFound, with: :not_found
   rescue_from ::RequestErrors::InternalServerError, with: :internal_server_error

   private

   def internal_server_error(error)
      logger.error(format_error(error))
      render json: error_serializer.serialize(error), status: :internal_server_error
   end

   def unprocessable_entity(error)
      logger.error(format_error(error))
      render json: error_serializer.serialize(error), status: :unprocessable_entity
   end

   def not_found(error)
      logger.error(format_error(error))
      render json: error_serializer.serialize(error), status: :not_found
   end

   def format_error(error)
      {
         error: error.class,
         message: error.message,
         backtrace: error.backtrace,
      }
   end

   def error_serializer
      ::ErrorSerializer.new
   end
end
