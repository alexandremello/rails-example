class UserCheckController < ApplicationController
   def check
      user_id = params[:user_id]
      amount = params[:amount]

      raise ::RequestErrors::UnprocessableEntity.new("amount must be a number") unless amount.is_a? Numeric

      balance = use_case.add_check_into_user_account(user_id, amount)

      render json: user_balance_serializer.serialize(balance), status: :ok
   rescue ::UseCaseErrors::UserNotFound => error
      raise ::RequestErrors::NotFound.new error.message
   rescue ::UseCaseErrors::NonSufficientFunds => error
      raise ::RequestErrors::UnprocessableEntity.new error.message
   end

   private

   def use_case
      ::AddCheckIntoUserAccount.new
   end

   def user_balance_serializer
      ::UserBalanceSerializer.new
   end
end
