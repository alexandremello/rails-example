class UserBalanceController < ApplicationController
   def balance
      user_id = params[:user_id]

      balance = use_case.balance_by_user_id(user_id)

      render json: user_balance_serializer.serialize(balance), status: :ok
   rescue ::ShowUserBalance::UserNotFound => error
      raise ::RequestErrors::NotFound.new error.message
   end

   private

   def use_case
      ::ShowUserBalance.new
   end

   def user_balance_serializer
      ::UserBalanceSerializer.new
   end
end
